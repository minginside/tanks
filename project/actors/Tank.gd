extends KinematicBody

signal died

const MOVE_ACCEL = 8.0
const MOVE_SPEED = 3.0
const ROTATE_SPEED = 6.0
const TURRET_SPEED = 4.0
const GRAVITY = 0.4
const SHOOT_DELAY = 1.0

var bullet = preload("res://actors/Bullet.tscn")
var explosion = preload("res://effects/Explosion.tscn")

onready var utils = $"/root/utils"

onready var turret = $Turret
onready var body = $Body
onready var bullet_spawn = $"Turret/GunEnd"
onready var health_label = $Health

var player_id = "key1"
var speed = 0.0
var health = 5
var cooldown = 0.0

func _ready():
    var player_number = {
        key1 = 1,
        key2 = 2,
        joy1 = 3,
        joy3 = 4,
    }[player_id]
    health_label.rect_position.x = 100 + 200 * (player_number - 1)
    health_label.text = "Health: %s" % health

    # Reset transform, apply it to body and turret instead
    turret.rotation.y = rotation.y
    body.rotation.y = rotation.y
    global_transform.basis = get_parent().global_transform.basis

func _physics_process(delta):
    # Read movement
    var move_vec = Vector2()
    move_vec.y -= Input.get_action_strength("%s_up" % player_id)
    move_vec.y += Input.get_action_strength("%s_down" % player_id)
    move_vec.x += Input.get_action_strength("%s_right" % player_id)
    move_vec.x -= Input.get_action_strength("%s_left" % player_id)
    if move_vec:
        move_vec *= 1.0 / max(1.0, move_vec.length())

    # Read aim
    var aim = 0.0
    aim -= Input.get_action_strength("%s_turn_clockwise" % player_id)
    aim += Input.get_action_strength("%s_turn_counter" % player_id)

    var shoot = Input.is_action_just_pressed("%s_shoot" % player_id)

    # Turn towards destination
    var chg = 0.0
    var speed_target = 0.0
    if move_vec.length_squared() > 0.1:
        var target_angle = atan2(-move_vec.x, -move_vec.y)
        chg = utils.angle_wrap(target_angle - body.rotation.y)
    if abs(chg) > 0.01:
        body.rotation.y += utils.angle_wrap(clamp(chg, -ROTATE_SPEED * delta, ROTATE_SPEED * delta))
        speed_target = max(0, cos(chg)) * MOVE_SPEED
    else:
        speed_target = 0.0
        if move_vec:
            speed_target = MOVE_SPEED

    # Update speed
    var speed_change = speed_target - speed
    if speed_change:
        speed_change *= min(1.0, MOVE_ACCEL * delta / abs(speed_change))
    speed += speed_change

    # Move
    move_and_slide(Vector3(-sin(body.rotation.y) * speed, 0.0, -cos(body.rotation.y) * speed), Vector3(0, 1, 0))

    # Turn turret
    turret.rotation.y += TURRET_SPEED * aim * delta

    # Shoot
    if cooldown > 0.0:
        cooldown -= delta
    if shoot and cooldown <= 0.0:
        var newbullet = bullet.instance()
        newbullet.global_transform.origin = bullet_spawn.global_transform.origin
        newbullet.rotation.y = turret.rotation.y
        get_parent().add_child(newbullet)
        $"Turret/ShootSound".play()
        cooldown = SHOOT_DELAY

func hit_by_bullet():
    health -= 1
    health_label.text = "Health: %s" % health
    if health <= 0:
        emit_signal("died")
        var expl = explosion.instance()
        expl.global_transform.origin = global_transform.origin
        get_parent().add_child(expl)
        queue_free()
    return true  # Indicates the bullet should be removed
