extends KinematicBody

const SPEED = 5.0
const LIFESPAN = 5.0
const DISAPPEAR = 3.0
const ROTATION_SPEED = 3.0

var disintegrate = preload("res://effects/Disintegrate.tscn")

onready var sphere = $Sphere

var life = LIFESPAN
onready var velocity = Vector3(0, 0, -SPEED).rotated(Vector3(0, 1, 0), rotation.y)

func blow_up():
    var dis = disintegrate.instance()
    dis.global_transform.origin = global_transform.origin
    get_parent().add_child(dis)
    queue_free()

func _physics_process(delta):
    # Die after some time
    life -= delta
    if life < 0.0:
        blow_up()

    # Rotate
    sphere.rotate(Vector3(1, 0, 0), -ROTATION_SPEED * delta)

    # Update alpha
    var mat = sphere.get_surface_material(0)
    mat.albedo_color.a = min(life, LIFESPAN - DISAPPEAR) / (LIFESPAN - DISAPPEAR)

    # Move and collide
    var collision = move_and_collide(velocity * delta)
    if collision:
        # Bounce on impact
        if collision.normal.dot(velocity) < 0.0:
            velocity -= 2 * velocity.project(collision.normal)

        # Affect collided objects
        if collision.collider.has_method("hit_by_bullet"):
            var rm = collision.collider.hit_by_bullet()
            if rm:
                blow_up()
