extends Spatial

const to_load = ["res://actors/Bullet.tscn", "res://effects/Disintegrate.tscn", "res://effects/Explosion.tscn"]

onready var loading_screen = $LoadingScreen
onready var root = $LoadRoot

var scenes = []

func _ready():
    AudioServer.set_bus_mute(0, true)

    for scene_name in to_load:
        var loaded_scene = load(scene_name)
        scenes.append(loaded_scene)
        var instanced_scene = loaded_scene.instance()
        root.add_child(instanced_scene)

    $SetupTimer.start()

func setup():
    AudioServer.set_bus_mute(0, false)

    # Load the game
    add_child(load("res://World.tscn").instance())

    $CleanupTimer.start()

func cleanup():
    # Delete the nodes we created
    root.queue_free()

    # KEEP references to the loaded scenes

    # Delete loading screen
    loading_screen.queue_free()
